package com.progressoft.induction.practices;

import java.util.Scanner;

public class Task2 {
    public static void main (String[]args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter a sentence: ");
        String line= sc.nextLine();



        String []arr =line.split("\\s");
        String capitalizeStr="";
        for(String sentence:arr){

            String fLetter=sentence.substring(0,1);
            String rLetters=sentence.substring(1);
            capitalizeStr = capitalizeStr + fLetter.toUpperCase()+rLetters+" ";
        }
        System.out.println(capitalizeStr);
    }

}


