package com.progressoft.induction.practices;

import java.util.Scanner;

public class Task3 {

public static void  main (String[]args){
    Scanner sc = new Scanner(System.in);

    System.out.print("Enter a sentence: ");
    String line= sc.nextLine();

    System.out.println(line.toLowerCase());
}
}
