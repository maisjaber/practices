package com.progressoft.induction.practices;

import java.util.Scanner;

public class Task5 {
    public  static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter a sentence: ");
        String line= sc.nextLine();

        String reverseWord = "";
        for (int j = line.length() - 1; j >= 0; j--) {

            reverseWord = reverseWord + line.charAt(j);

        }
        System.out.println(reverseWord);
    }
}
